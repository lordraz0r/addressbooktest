<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('contacts', 'ContactController');
Route::get('/contacts/{contact_id}/delete', 'ContactController@delete')->name('contacts.delete');

Route::post('/contacts/{contact_id}/emails', 'ContactController@storeEmail')->name('contact_emails.store');
Route::post('/contacts/{contact_id}/numbers', 'ContactController@storeNumber')->name('contact_numbers.store');

Route::get('/home', 'HomeController@index')->name('home');
