<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //

    public function email_addresses()
    {
        return $this->hasMany('App\ContactEmailAddress');
    }

    public function numbers()
    {
        return $this->hasMany('App\ContactNumber');
    }
}
