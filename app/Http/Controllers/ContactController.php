<?php

namespace App\Http\Controllers;

use App\Contact;
use App\ContactEmailAddress;
use App\ContactNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::paginate(10);
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
        ])->validate();

        $contact = new Contact();

        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;

        $contact->save();

        return redirect('contacts');
    }

    public function storeEmail(Request $request, $contact_id)
    {
        Validator::make($request->all(), [
            'email' => 'required|email',
        ])->validate();

        $contact_email = new ContactEmailAddress();
        
        $contact_email->contact_id = $contact_id;
        $contact_email->email = $request->email;

        $contact_email->save();

        return redirect('contacts');
    }

    public function storeNumber(Request $request, $contact_id)
    {
        Validator::make($request->all(), [
            'number' => 'required'
        ])->validate();

        $contact_number = new ContactNumber();

        $contact_number->contact_id = $contact_id;
        $contact_number->phone_number = $request->number;

        $contact_number->save();

        return redirect('contacts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
        ])->validate();

        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;

        $contact->save();

        return redirect('contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function delete($contact_id)
    {
        $contact = Contact::find($contact_id);
        $contact->delete();
        return redirect('contacts');
    }
}
