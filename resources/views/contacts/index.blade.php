@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Contact
                    <a href="{{route('contacts.create')}}" class="btn btn-primary btn-sm float-right" role="button">Create New</a>
                </div>

                <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($contacts as $contact)
                        <tr>
                            <th scope="row">{{ $contact->id}}</th>
                            <td>{{ $contact->first_name}}</td>
                            <td>{{ $contact->last_name}}</td>
                            <td>
                                <a href="{{route('contacts.edit', $contact->id)}}" class="btn btn-primary btn-sm" role="button"><i class="fas fa-pencil-alt fa-xs"></i></i></a>
                                <a href="{{route('contacts.delete', $contact->id)}}" class="btn btn-danger btn-sm" role="button"><i class="fas fa-trash-alt fa-xs"></i></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td>No Contacts Found</td>
                        </tr>
                        @endforelse
                        
                    </tbody>
                    </table>
                    {{ $contacts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
